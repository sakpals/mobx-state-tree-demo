import React from "react";
import OrderList from "./components/OrderList";
import "./App.css";

function App() {
  return (
    <div>
      <OrderList />
    </div>
  );
}

export default App;
