import Coffee from "../Coffee";

it("creates Coffee", () => {
  const cappucino = Coffee.create({
    type: "cappucino",
    name: "sampada",
    sugar: 1,
    milk: true
  });

  expect(cappucino.milkPreference).toEqual("milk");
  expect(cappucino.sugar).toEqual(1);
});

it(" adds sugar to black coffee", () => {
  const black = Coffee.create({
    type: "black",
    name: "joe"
  });
  expect(black.sugarPreference).toEqual("no sugar");
  black.addSugar();
  expect(black.sugarPreference).toEqual("1 sugar(s)");
  black.addSugar();
  expect(black.sugar).toEqual(2);
});
