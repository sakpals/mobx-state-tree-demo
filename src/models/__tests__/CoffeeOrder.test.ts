import { getSnapshot } from "mobx-state-tree";
import CoffeeOrder from "../CoffeeOrder";

it("creates a coffee order with one coffee and then add request", () => {
  const order = CoffeeOrder.create({
    orders: [
      {
        type: "cappucino",
        name: "sampada",
        sugar: 1,
        milk: true
      }
    ]
  });

  expect(getSnapshot(order)).toEqual({
    orders: [
      {
        type: "cappucino",
        name: "sampada",
        sugar: 1,
        milk: true
      }
    ],
    request: "",
    sent: false
  });

  // add a request
  order.addRequest("make it quick.");

  expect(order.request).toEqual("make it quick.");
});
