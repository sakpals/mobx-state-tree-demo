import { types } from "mobx-state-tree";
import Coffee from "./Coffee";

const CoffeeOrder = types
  .model("CoffeeOrders", {
    orders: types.array(Coffee),
    request: types.optional(types.string, ""),
    sent: false
  })
  // allows you to modify a node, NOTE: no `this`
  .actions(self => ({
    sendOrder() {
      self.sent = true;
    },
    addRequest(newRequest: string) {
      self.request = newRequest;
    },
    removeOrder(order: any) {
      self.orders.splice(self.orders.indexOf(order), 1);
    }
  }));

export default CoffeeOrder;
