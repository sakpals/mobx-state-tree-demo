import { types, getParent, Instance } from "mobx-state-tree";
import CoffeeOrder from "./CoffeeOrder";

const Coffee = types
  // corresponds to a node
  .model("Coffee", {
    type: types.string, // corresponds to a leaf
    name: types.string,
    sugar: types.optional(types.number, 0), // could also just assign 0
    milk: false
  })
  .actions(self => ({
    addSugar() {
      self.sugar += 1;
    },
    reduceSugar() {
      if (self.sugar > 0) {
        self.sugar -= 1;
      }
    },
    addMilk() {
      self.milk = true;
    },
    removeMilk() {
      self.milk = false;
    },
    remove() {
      (getParent(self, 2) as Instance<typeof CoffeeOrder>).removeOrder(self);
    }
  }))
  .views(self => ({
    get sugarPreference() {
      return self.sugar > 0 ? `${self.sugar} sugar(s)` : "no sugar";
    },
    get milkPreference() {
      return self.milk ? "milk" : "no milk";
    }
  }));

export default Coffee;
