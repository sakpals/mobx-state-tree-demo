import React from "react";
import { observer } from "mobx-react";
import CoffeeOrder from "../models/CoffeeOrder";
import Coffee from "../models/Coffee";
import Order from "../components/Order";
import { onPatch, getSnapshot } from "mobx-state-tree";
import makeInspectable from "mobx-devtools-mst";
/* TODO:
- show time travel with snapshots, this will also demo how snapshots can be converted to state
*/
const order = CoffeeOrder.create({
  orders: [
    Coffee.create({
      type: "Flat White",
      name: "Jack",
      sugar: 2
    }),
    // you can also omit .create() and just provide a Snapshot since it can infer type
    { type: "Mocha", name: "Jill", milk: true }
  ],
  sent: false
});

makeInspectable(order);

@observer
class OrderList extends React.Component {
  render() {
    console.log("Snapshot: ", getSnapshot(order));
    return (
      <div style={{ textAlign: "center" }}>
        <h1>ORDER</h1>
        {order.orders.map((o, i) => (
          <Order order={o} key={i} />
        ))}
        {!order.sent ? (
          <button onClick={order.sendOrder}>Submit</button>
        ) : (
          "SENT!"
        )}
      </div>
    );
  }
}

// Patches describe what mutations occurred to the model (in this case `order`)
onPatch(order, patch => {
  console.log("patch for Order node: ", patch);
});

// remove observer for DEMO and show how it works
export default OrderList;
