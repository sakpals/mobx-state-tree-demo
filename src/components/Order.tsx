import React from "react";

interface OrderProps {
  name: string;
  type: string;
  sugar?: number;
  milk?: boolean;
  sugarPreference?: () => any;
  milkPreference?: () => any;
  addSugar?: () => any;
  reduceSugar?: () => any;
  addMilk?: () => any;
  removeMilk?: () => any;
  remove?: () => any;
}

interface IProps {
  order: OrderProps;
}

// show selective rendering based on observerable properties (i.e. get rid of name and update name to show how it doesn't re-render)
const Order = (props: IProps) => {
  const {
    name,
    type,
    sugar,
    milk,
    sugarPreference,
    milkPreference,
    addSugar,
    reduceSugar,
    addMilk,
    removeMilk,
    remove
  } = props.order;
  return (
    <div>
      <span>
        <button onClick={remove}>Remove</button>
        {`${type} with ${sugarPreference} and ${milkPreference} for ${name}`}
      </span>
      <br />

      <p>
        <button onClick={addSugar}>+</button>Sugar
        <button onClick={reduceSugar} disabled={sugar === 0}>
          -
        </button>
        {milk ? (
          <button onClick={removeMilk}>Remove milk</button>
        ) : (
          <button onClick={addMilk}>Add milk</button>
        )}
      </p>
    </div>
  );
};

export default Order;
